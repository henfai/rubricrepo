<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_kboyle
 * @copyright   Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_delete_category_instance extends moodleform{
	function definition(){
        global $CFG, $DB, $USER;
        //$this->page->requires->js_init_call('M.local_rubricrepo_sgannon1.init',array('this is the param1 value'), false, $jsmodule);
        $mform = $this ->_form;
        $table1 = 'category';
        $formid = $_GET['id'];
        $categories = array();
        $category = $DB->get_records($table1, array('form'=>$formid));
        foreach($category as $c){
            if ($c->posneg == 0){
                $categories[$c->id] = $c->name.' (Positive)' ;

            }else{
                $categories[$c->id] = $c->.' (Negative)' ;
            }
        }
        $mform->addElement('select','deleteCategory', get_string('classification', 'local_rubricrepo_sgannon1'), $categories);
        $selectgroup[] = $mform->createElement('submit', 'deleteCategory','Accept');
    }

};


?>
