<?php
/**
 * Handles upgrading the plug-in
 *
 * @package    local
 * @subpackage feedback_ec10
 * @copyright  Eric Cheng ec10@ualberta.ca
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_feedback_ec10_upgrade($oldversion) {

    global $CFG, $USER, $DB, $OUTPUT;

    require_once($CFG->dirroot.'/lib/db/upgradelib.php');

    $dbman = $DB->get_manager();

   /**
    * Version 2015111701 adds new columns to the feedback_form to indicate who owns the form and the visibility of the form.
    * The owner is identified by the user ID and is a foreign key reference.
    */
    if ($oldversion < 2015111701) {
          // Define field userid to be added to feedback_form.
        $table = new xmldb_table('feedback_form');
        $field = new xmldb_field('userid', XMLDB_TYPE_INTEGER, '10', null, null, null, null, 'description');

        // Conditionally launch add field userid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

      // Define key userid (foreign) to be added to feedback_form.
        $table = new xmldb_table('feedback_form');
        $key = new xmldb_key('userid', XMLDB_KEY_FOREIGN, array('userid'), 'user', array('id'));

        // Launch add key userid.
        $dbman->add_key($table, $key);

        // Define field visibility to be added to feedback_form.
        $table = new xmldb_table('feedback_form');
        $field = new xmldb_field('visibility', XMLDB_TYPE_BINARY, null, null, null, null, null, 'userid');

        // Conditionally launch add field visibility.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Feedback_ec10 savepoint reached.
        upgrade_plugin_savepoint(true, 2015111701, 'local', 'feedback_ec10');

    }

    /**
     * Version 2015110601 adds a new table to save the free-text box comments and new columns to allow the form
     * to reference the corrent student, post, and instructor.
     */
    if ($oldversion < 2015110601) {
         // Define table saved_freetxt to be created.
        $table = new xmldb_table('saved_freetxt');

        // Adding fields to table saved_freetxt.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('comment_txt', XMLDB_TYPE_TEXT, null, null, null, null, null);
        $table->add_field('savedform_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table saved_freetxt.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('savedform_id', XMLDB_KEY_FOREIGN, array('savedform_id'), 'saved_form', array('id'));

        // Conditionally launch create table for saved_freetxt.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define field studentid to be added to saved_form.
        $table = new xmldb_table('saved_form');
        $field = new xmldb_field('studentid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'id');

        // Conditionally launch add field studentid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // Define key studentid (foreign) to be added to saved_form.
        $key = new xmldb_key('studentid', XMLDB_KEY_FOREIGN, array('studentid'), 'user', array('id'));

        // Launch add key studentid.
        $dbman->add_key($table, $key);

         // Define field instructorid to be added to saved_form.
        $table = new xmldb_table('saved_form');
        $field = new xmldb_field('instructorid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'studentid');

        // Conditionally launch add field instructorid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
         // Define key instructorid (foreign) to be added to saved_form.
        $key = new xmldb_key('instructorid', XMLDB_KEY_FOREIGN, array('instructorid'), 'user', array('id'));

        // Launch add key instructorid.
        $dbman->add_key($table, $key);

        // Define field postid to be added to saved_form.
        $table = new xmldb_table('saved_form');
        $field = new xmldb_field('postid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null, 'instructorid');

        // Conditionally launch add field postid.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // Define key postid (foreign) to be added to saved_form.
        $key = new xmldb_key('postid', XMLDB_KEY_FOREIGN, array('postid'), 'forum_posts', array('id'));

        // Launch add key postid.
        $dbman->add_key($table, $key);

        // Feedback_ec10 savepoint reached.
        upgrade_plugin_savepoint(true, 2015110601, 'local', 'feedback_ec10');
    }


    /**
     * Version 2015110301 adds tables for saving the completed form data 
     */
    if ($oldversion < 2015110301) {

        // Define table saved_form to be created.
        $table = new xmldb_table('saved_form');

        // Adding fields to table saved_form.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);

        // Adding keys to table saved_form.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

        // Conditionally launch create table for saved_form.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Define table saved_category to be created.
        $table = new xmldb_table('saved_category');

        // Adding fields to table saved_category.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('savedform_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('category_ref_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table saved_category.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('savedform_id', XMLDB_KEY_FOREIGN, array('savedform_id'), 'saved_form', array('id'));
        $table->add_key('category_ref_id', XMLDB_KEY_FOREIGN, array('category_ref_id'), 'category', array('id'));

        // Conditionally launch create table for saved_category.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

         // Define table saved_comment to be created.
        $table = new xmldb_table('saved_comment');

        // Adding fields to table saved_comment.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('savedcategory_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('comment_ref_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);

        // Adding keys to table saved_comment.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('savedcategory_id', XMLDB_KEY_FOREIGN, array('savedcategory_id'), 'saved_category', array('id'));
        $table->add_key('comment_ref_id', XMLDB_KEY_FOREIGN, array('comment_ref_id'), 'comments', array('id'));

        // Conditionally launch create table for saved_comment.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        // Feedback_ec10 savepoint reached.
        upgrade_plugin_savepoint(true, 2015110301, 'local', 'feedback_ec10');

    }
}