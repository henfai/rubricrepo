<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Initial page for the plug-in
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

global $PAGE, $CFG, $DB;
require_once('../../config.php');

require_login();
require_capability('local/feedback_ec10:add', context_system::instance());
//require_once($CFG->dirroot.'/local/feedback_ec10/feedback_form.php');
require_once($CFG->dirroot.'/local/feedback_ec10/forum_search_engines.php');
require_once($CFG->dirroot.'/local/feedback_ec10/rubric_manager_form.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_ec10'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_ec10'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_ec10/view.php');
$PAGE->requires->js_init_call('M.local_feedback_ec10.init');

$table1 = 'forum';
$table2 = 'forum_discussions';

$rubric_manager = new rubric_manager();
$course_search = new create_course_list();

/**
 * This block handles whether or not the discussion search engine should be displayed based on the header POST information AND if there are any discussions
 * that corresponds to the course selected.
 */
if (!$_GET['id']) {
	$PAGE->set_url($CFG->wwwroot.'/local/feedback_ec10/view.php');
} else {
		$PAGE->set_url($CFG->wwwroot.'/local/feedback_ec10/view.php?id='.$_GET['id']);	
		if ($DB->get_records($table1, array('course'=>$_GET['id'], 'type'=>'general'))) {
			$discussion_search = new discussion_forum_search($CFG->wwwroot.'/local/feedback_ec10/view.php?id='.$_GET['id']);
	}
}

/**
 * This block handles the logic for how to handle user submissions when selecting the course and discussion, and how to display them.
 * 1. If a course is selected, re-direct to itself, and post the course ID.
 * 2. If a discussion forum is selected, re-direct to itself, and post the course ID and forum ID.
 * 3. Display the page based on the post information.
 */
if ($_POST['search']) {
	$data = $course_search->get_data();
	redirect($CFG->wwwroot.'/local/feedback_ec10/view.php?id='.$data->course);
	
} else if ($_POST['submitbuttonforum']) {
	$data = $discussion_search->get_data();
	redirect($CFG->wwwroot.'/local/feedback_ec10/view.php?id='.$_GET['id'].'&fid='.$data->forum_search_engine);
} else {
	echo $OUTPUT->header();
	$rubric_manager->display();
	$course_search->display();
	if($_GET['id']) {
		$discussion_search->display();
	}
	if ($_GET['fid'] && $DB->get_records($table2, array('forum'=>$_GET['fid']))) {
		display_thread_search();
	}
	echo $OUTPUT->footer();
}

?>
