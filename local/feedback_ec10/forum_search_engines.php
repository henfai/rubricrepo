<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The search engines used by 'feedback_ec10'
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca && Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
//echo $OUTPUT->get_string('writing', 'local_feedback_ec10');
require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';


/**
 * The search engine that allows the user to select the course they are marking.
 */
class create_course_list extends moodleform {
	function definition() {
		global $CFG, $COURSE, $DB, $USER;
        $mform = $this->_form;
        $attributes_heading = array('size' => '30');
        $attributes =array('size'=>'18', 'maxlength' => '100');
        $mform->addElement('header', 'courseheader', get_string('useforms', 'local_feedback_ec10'));      
        
        /*Get enrolled courses of user by user id
            Source: http://codeboxr.com/blogs/moodle-development-tips-tricks-part1
            Author: Sabju Kundu
        */
        $user_courses = enrol_get_users_courses($USER->id, true, '*', 'visible DESC,sortorder ASC');
        $select_course_array=array();
        $select_course_array[0]=''; // Add a blank space to the select box.
        foreach ($user_courses as $uc){
            $select_course_array[$uc->id] = $uc->fullname;
        }

        $selectgroup = array();
        $selectgroup[] = $mform->createElement('select', 'course', get_string('selectcourse', 'local_feedback_ec10'), $select_course_array, array('style'=>'min-width:200px; max-width:200px;'));
        if ($_GET['id']) {
            $mform->setDefault('course', $_GET['id']); //Set the default selection box display based on the posted ID.
        }
        $selectgroup[] = $mform->createElement('submit', 'search', get_string('coursesearch', 'local_feedback_ec10'));
        $mform->addElement('group', 'courseselect', get_string('selectcourse', 'local_feedback_ec10'), $selectgroup, array('  '), false);
    }
}

/**
 * The search engine that allows the user to select discussion forum that they are marking.  The discussion forum must be of type=='general'
 */
class discussion_forum_search extends moodleform {

    function definition() {
        global $CFG, $DB;
        $mform = $this->_form;
        $table1 = 'forum';
        $id = $_GET['id'];
        $results = $DB->get_records($table1, array('course'=>$id, 'type'=>'general'));
        $select_array = array();
        $select_array[0] = '';

        foreach($results as $r) {
            //echo $r->id;
            //echo $r->name;
            $select_array[$r->id] = $r->name;
        } 

        $element_group = array();
        $mform->addElement('header', 'header1', get_string('useforum', 'local_feedback_ec10'));
        $element_group[] = $mform->createElement('select', 'forum_search_engine', get_string('forumlist', 'local_feedback_ec10'), $select_array, array('style'=>'min-width:200px; max-width:200px;'));
        if($_GET['fid']) {
            $mform->setDefault('forum_search_engine', $_GET['fid']); //Set the default selection box display based on the posted ID.
        }
        $element_group[] = $mform->createElement('submit', 'submitbuttonforum', get_string('forumsearch', 'local_feedback_ec10'));
        $mform->addElement('group', 'forumsearchengine', get_string('selectforum', 'local_feedback_ec10'), $element_group, array('   '), false);

    }
}
/**
 * Defunct method, has been re-factored to 'display_thread_search()'
 */
class thread_search_engine extends moodleform {
	function definition() {
		global $DB, $CFG;
		$mform = $this->_form;
		$attributes_heading = array('size' => '30');
		$fid = $_GET['fid'];
		$mform->addElement('header', 'heading3', get_string('postheading', 'local_feedback_ec10'), $attributes_heading);
        $table2 = 'forum_discussions';
        $user_table = 'user';
        $posts = $DB->get_records($table2, array('forum'=>$fid));
        $table = new html_table();
        $table->head = array('   Post Title   ', '   Author   ');

        //print_r($posts);
        //echo '<br><br>';
        foreach($posts as $p) {
            $post_id = $p->id;
            $post_title = $p->name;
            $author_id = $p->userid;
            $select = "id ='".$author_id."'";
            $author_info = $DB->get_records_select($user_table, $select);
            $author_firstname = $author_info[$author_id]->firstname;
            $author_lastname = $author_info[$author_id]->lastname;
            $author_fullname = $author_firstname.' '.$author_lastname;
            $table->data[] = array('<a href=view.php?id='.$post_id.'>'.$post_title.'</a>', $author_fullname);
        }

        $mform->addElement('html', $table);
        //echo html_writer::table($table);
	}
}

/**
 * This function formats and displays all the threads for a discussion forum for the user to search through.
 */
function display_thread_search() {
	global $DB, $CFG;
		//$mform = $this->_form;
	$attributes_heading = array('size' => '30');
	$fid = $_GET['fid'];
		//$mform->addElement('header', 'heading3', get_string('postheading', 'local_feedback_ec10'), $attributes_heading);
    $table2 = 'forum_discussions';
    $user_table = 'user';
    $posts = $DB->get_records($table2, array('forum'=>$fid));
    $table = new html_table();
    $table->head = array('   Post Title   ', '   Author   ');
    $table->tablealign = 'center';
    $table->width = '75%';

        //print_r($posts);
        //echo '<br><br>';
    foreach($posts as $p) {
        $post_id = $p->firstpost;
        $post_title = $p->name;
        $author_id = $p->userid;
        $select = "id ='".$author_id."'";
        $author_info = $DB->get_records_select($user_table, $select);
        $author_firstname = $author_info[$author_id]->firstname;
        $author_lastname = $author_info[$author_id]->lastname;
        $author_fullname = $author_firstname.' '.$author_lastname;
        $table->data[] = array('<a href=feedback.php?id='.$post_id.'>'.$post_title.'</a>', $author_fullname);
    }

    echo html_writer::table($table);
}
?>
