<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_kboyle
 * @copyright   Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */


global $CFG, $PAGE, $DB, $USER;
 
require_once('../../config.php');

require_login();
require_capability('local/rubricrepo_kboyle:add', context_system::instance());
//require_once($CFG->dirroot.'/local/rubricrepo_kboyle/email_form.php');
require_once($CFG->dirroot.'/local/rubricrepo_kboyle/edit_form.php');
//require_once($CFG->dirroot.'/local/rubricrepo_kboyle/delete_category_form.php');

/*
require_once($CFG->dirroot.'/local/rubricrepo_kboyle/delete_comment_form.php');
require_once($CFG->dirroot.'/local/rubricrepo_kboyle/add_comment_form.php');
require_once($CFG->dirroot.'/local/rubricrepo_kboyle/add_category_form.php');
require_once($CFG->dirroot.'/local/rubricrepo_kboyle/change_status_form.php');
*/

//$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_rubricrepo_kboyle'));
$PAGE->set_heading(get_string('pluginname', 'local_rubricrepo_kboyle'));
$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php');
$edit_form = new create_edit_instance();
$changestatus_form = new create_changePermissions_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$_GET['id'].'&actionNo='.$_GET['actionNo']);
$createComm_form = new create_newComments_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$_GET['id'].'&actionNo='.$_GET['actionNo']);
//$email_form = new create_email_instance();
$createCat_form = new create_newCategory_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$_GET['id'].'&actionNo='.$_GET['actionNo']);
$formtable = 'feedback_form';
$categorytable = 'category';



if(!$_GET['id'] and !$_GET['editAction']){
	$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php');
}



if($_POST['editFormButton']){
	$data = $edit_form->get_data();
	redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$data->forms.'&actionNo='.$data->editAction);
}elseif($_POST['addChangePermissionsButton']){
	$formid = $_GET['id'];
	$formstuff =$DB->get_record($formtable, array('id'=>$formid)); 
	if($formstuff->visibility == 0){
		$vis = 1;

	}else{
		$vis = 0;

	}
	$dataobject = new stdClass();
	$dataobject->id = $_GET['id'];
	$dataobject->title;
	$dataobject->visibility = $vis;
	$dataobject->userid;
	$DB->update_record($formtable, $dataobject, $bulk=false);
	redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/view.php?'.$visibilityData->visibility);
	
	//redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$data->forms.'&actionNo='.$data->editAction);
}elseif($_POST['addCommentsButton']){
	$commentsTable = 'comments';
	$data = $createComm_form->get_data();
	$comments = $data->comment;  
	$categoryAssociations = $data->categorySelect;
	$i = 0;
	foreach ($comments as $comment) {
		$categoryAssociation = $categoryAssociations[$i];
		$commentRecord = new stdClass();
		$commentRecord->comment_text = $comment ;
		$commentRecord->category = $categoryAssociation; 
		$DB->insert_record($commentsTable, $commentRecord ,$returnid=false, $bulk=false);
		$i = $i + 1;
	}
	//redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/view.php?');
	redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/preview.php?id='.$_GET['id']);

}elseif($_POST['addCategoryButton']){

	$formid = $_GET['id'];
	$categorytable = 'category';
	$commentsTable = 'comments';
	
	$data = $createCat_form->get_data();
	$newCategoryName = $data->newCategoryTitle;
	$newCategoryClassification = $data->proscons;
	$categoryRecord =  new stdClass();
	$categoryRecord->name = $newCategoryName;
	$categoryRecord->form = $formid;
	$categoryRecord->posneg = $newCategoryClassification;
	$categoryID = $DB->insert_record($categorytable, $categoryRecord ,$returnid=true, $bulk=false);
	$comments = $data->newCategoryComment;
	$i = 0;
	foreach ($comments as $comment) {
		$commentRecord = new stdClass();
		$commentRecord->comment_text = $comment ;
		$commentRecord->category = $categoryID ; 
		$DB->insert_record($commentsTable, $commentRecord ,$returnid=false, $bulk=false);
		$i = $i + 1;
	}



	redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/preview.php?id='.$_GET['id']);

}else{

	echo $OUTPUT->header();
	$edit_form->display();

	if($_GET['actionNo'] == 1){
		$createCat_form = new create_newCategory_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$_GET['id'].'&actionNo='.$_GET['actionNo']);
		$createCat_form ->display();
		//$edit_form = new create_edit_instance();
		//$edit_form->display();
		//echo "neat";
	}else if($_GET['actionNo'] == 2){
		$createComm_form = new create_newComments_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$_GET['id'].'&actionNo='.$_GET['actionNo']);
		$createComm_form ->display();
		//$edit_form = new create_edit_instance();
		//$edit_form->display();
		//echo "neat";
	}else if($_GET['actionNo'] == 3){
		$deleteCat_form = new create_deleteCategory_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$_GET['id'].'&actionNo='.$_GET['actionNo']);
		$deleteCat_form ->display();
		//$edit_form = new create_edit_instance();
		//$edit_form->display();
		//echo "neat";
	}else if($_GET['actionNo'] == 4){
		$deleteComm_form = new create_deleteComment_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/edit.php?id='.$_GET['id'].'&actionNo='.$_GET['actionNo']);
		$deleteComm_form->display();
		//$edit_form = new create_edit_instance();
		//$edit_form->display();
		//echo "neat";
	}else if($_GET['actionNo'] == 5){
		$changestatus_form ->display();
		//$edit_form = new create_edit_instance();
		//$edit_form->display();
		//echo "neat";
	}
	

	echo $OUTPUT->footer();
}



//echo $OUTPUT->header();
//$deleteCat_form->display();


?>