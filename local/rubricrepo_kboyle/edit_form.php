<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_kboyle
 * @copyright   Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
require_login();
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_edit_instance extends moodleform{
	function definition(){
	   global $CFG, $DB, $USER;
       //$formid = $_GET['id']
       //$actionId = $_GET['actionNo'];
       //$this->page->requires->js_init_call('M.local_rubricrepo_kboyle.init',array('this is the param1 value'), false, $jsmodule); 
       $formid = $_GET['id'];
       $actionid = $_GET['actionNo'];
     $mform = $this ->_form;
     $actionArray = array(1=>'Add Category',2=>'Add Comment',3=>'Delete Category',4=>'Delete Comment',5=>'Change Visibility');
       //$this->page->requires->js_init_call('M.local_rubricrepo_kboyle.init',array('this is the param1 value'), false, $jsmodule); 
       //$mfrom->addElement('text', 'formName', get_string('formName', 'local_rubricrepo_kboyle'));
         //$mform->addElement('header','formdescription',get_string('formInfo', 'local_rubricrepo_kboyle'));
     $forms = array();
     $userid = $USER->id;
     $mform = $this ->_form;
     $formtable = 'feedback_form';
     $form_ids = $DB->get_records($formtable, array('userid'=>$userid));
     foreach($form_ids as $f) {
       $forms[$f->id] = $f->title;
     }
     $mform->addElement('header','editInfoHeader',get_string('editInfo', 'local_rubricrepo_kboyle'));
     $mform->addElement('select','forms', get_string('formEdit', 'local_rubricrepo_kboyle'), $forms,array('style'=>'min-width:200px; max-width:200px;'));
     $mform->addElement('select','editAction', get_string('editType', 'local_rubricrepo_kboyle'), $actionArray,array('style'=>'min-width:200px; max-width:200px;'));

     if($formid != 0 && $actionid != 0){
        $mform->setDefault('forms',$formid);
        $mform->setDefault('editAction', $actionid);

     }

     //$radioarray= array();

       //$mform->addElement('header','description',get_string('addCategoryDesc', 'local_rubricrepo_kboyle'));
     //$mform->addElement('header', 'categories',get_string('numCategories', 'local_rubricrepo_kboyle'));
     //$mform->addElement('textarea', 'numCategories', get_string('numCategories', 'local_rubricrepo_kboyle'), 'wrap="virtual" rows="10" cols="60" resize="none" style="resize:none"');
     //$repeatarray = array();
     ///$repeatarray[] = $mform->createElement('text', 'category', get_string('student', 'local_rubricrepo_kboyle'));
     //$repeatarray[] = $mform->createElement('select','proscons', get_string('classification', 'local_rubricrepo_kboyle'), $procon);
     ///$repeatarray[] = $mform->createElement('button','addComment', 'Add comment');
     ///$repeatarray[] = $mform->createElement('text', 'option', get_string('optionno', 'choice'));
     //$repeatableoptions = array();
     //$repeateloptions['category']['default'] = '';
     //$mform->setType('option', PARAM_CLEANHTML);
     //$mform->setType('optionid', PARAM_INT);
     //$repeatno = 1;

     //$this->repeat_elements($repeatarray, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields', 1, get_string('numCategories', 'local_rubricrepo_kboyle'), false);
     //$this->add_action_buttons($cancel=true, $sumitlabel = get_string('nextPage', 'local_rubricrepo_kboyle'));
     $mform->addElement('submit', 'editFormButton',get_string('acceptText', 'local_rubricrepo_kboyle'));

    }

};

class create_deleteCategory_instance extends moodleform{
    function definition(){
        global $CFG, $DB, $USER;
        //$this->page->requires->js_init_call('M.local_rubricrepo_kboyle.init',array('this is the param1 value'), false, $jsmodule); 
        $mform = $this ->_form;
        $table1 = 'category';
        $formid = $_GET['id'];
        
        $categories = array(); 
        $category = $DB->get_records($table1, array('form'=>$formid));

        foreach($category as $c){

            if ($c->posneg == 0){
                $categories[$c->id] = $c->name.' (Positive)' ;

            }else{
                $categories[$c->id] = $c->name.' (Negative)' ;
            }            
        }
        $mform->addElement('header','deleteCategoryHeader',get_string('deleteCategoryHeader', 'local_rubricrepo_kboyle'));
        $mform->addElement('select','deleteCategory', get_string('student', 'local_rubricrepo_kboyle'), $categories,array('style'=>'min-width:200px; max-width:200px;'));
        $mform->addElement('submit', 'deleteCategoryButton',get_string('deleteRubric', 'local_rubricrepo_kboyle'));
        
    }

};

class create_deleteComment_instance extends moodleform{
    function definition(){
        global $CFG, $DB, $USER;
        //$this->page->requires->js_init_call('M.local_rubricrepo_kboyle.init',array('this is the param1 value'), false, $jsmodule); 
        $mform = $this ->_form;
        $categories_table = 'category';
        $comments_table = 'comments';
        $formid = $_GET['id'];
        
        $comments = array(); 
        $category = $DB->get_records($categories_table, array('form'=>$formid));



        foreach($category as $c){
            $allComments = $DB->get_records($comments_table, array('category'=>$c->id));

            foreach($allComments as $comm){

                //$comments[$comm->id] = ' ('.$c->name.' .)'.$comm->comment_text ;

                if ($c->posneg == 0){
                    //$categories[$c->id] = $c->name.' (Positive)' ;
                    $comments[$comm->id] = ' ('.$c->name.' Positive) '.$comm->comment_text ;

                }else{
                    $comments[$comm->id] = ' ('.$c->name.' Negative) '.$comm->comment_text ;
                }   
            }
        }
        
        $mform->addElement('select','deleteComment', get_string('classification', 'local_rubricrepo_kboyle'), $comments,array('style'=>'min-width:200px; max-width:400px;'));
        $mform->addElement('submit', 'deleteCommentButton',get_string('deleteRubric', 'local_rubricrepo_kboyle'));
    }

};


class create_newCategory_instance extends moodleform{
    function definition(){
       global $CFG, $DB, $USER;
       //$this->page->requires->js_init_call('M.local_rubricrepo_kboyle.init',array('this is the param1 value'), false, $jsmodule); 
       $formid = $_GET['id'];
       $mform = $this ->_form;
       $mform->addElement('header','addCategoryHeader',get_string('addCategoryHeader', 'local_rubricrepo_kboyle'));
       $mform->addElement('text', 'newCategoryTitle', get_string('newCategoryTitle', 'local_rubricrepo_kboyle'),array('style'=>'min-width:200px; max-width:200px;'));
       $procon = array(0=>'Positive',1=>'Negative');
       $mform->addElement('select','proscons', get_string('classification', 'local_rubricrepo_kboyle'), $procon,array('style'=>'min-width:200px; max-width:200px;'));

       $mform->addElement('header','addComments',get_string('addComments', 'local_rubricrepo_kboyle'));
       $repeatarray = array();
       $repeatarray[] = $mform->createElement('text', 'newCategoryComment', get_string('commenting', 'local_rubricrepo_kboyle'),array('style'=>'min-width:200px; max-width:200px;'));

       $repeatableoptions = array();
       $repeateloptions['newCategoryComment']['default'] = '';
       //$mfo
       $repeatno = 1;

       $this->repeat_elements($repeatarray, $repeatno, $repeateloptions, 'option_repeats', 'option_add_fields', 1, get_string('addComment', 'local_rubricrepo_kboyle'), false);
       //$this->add_action_buttons($cancel=true, $sumitlabel = get_string('nextPage', 'local_rubricrepo_kboyle'));
       $mform->addElement('submit', 'addCategoryButton',get_string('addInfoToForm', 'local_rubricrepo_kboyle'));

    }

};

class create_newComments_instance extends moodleform{
    function definition(){
        global $CFG, $DB, $USER;
        $mform = $this ->_form;
        $formid = $_GET['id'];
        $formtable = 'feedback_form';
        $categorytable = 'category';
        $repeatno = 1;

        $form = $DB->get_record($formtable,array('id'=>$_GET['id']));
        $formname = $form->title;
        $categoryarray= array();
        $categories = $DB->get_records($categorytable,array('form'=>$_GET['id']));
        foreach ($categories as $cat) {
            if($cat->posneg == 0){
                $categoryarray[$cat->id] = $cat->name.' (Positive)';
            }else{
                $categoryarray[$cat->id] = $cat->name.' (Negative)';

            }
        }

        $mform->addElement('header','addCommentsHeader',get_string('addCommentsHeader', 'local_rubricrepo_kboyle'));
        $repeatarray2 = array();
        //$mform->addElement('header','definedCategory',$formname);
        $repeatarray2[] = $mform->createElement('text', 'comment', get_string('commenting', 'local_rubricrepo_kboyle'),array('style'=>'min-width:200px; max-width:200px;'));
        $repeatarray2[] = $mform->createElement('select','categorySelect', get_string('association', 'local_rubricrepo_kboyle'), $categoryarray,array('style'=>'min-width:200px; max-width:200px;'));
        $repeatableoptions = array();
        $repeateloptions['category']['default'] = '';

        $this->repeat_elements($repeatarray2, $repeatno, $repeateloptions, 'option_repeats2', 'option_add_fields', 1, get_string('addComment', 'local_rubricrepo_kboyle'), false);
        $mform->addElement('submit', 'addCommentsButton', get_string('addInfoToForm', 'local_rubricrepo_kboyle'));
        //$this->add_action_buttons($cancel=true, $sumitlabel = get_string('saveIt', 'local_rubricrepo_kboyle'));
    }

};

class create_changePermissions_instance extends moodleform{
    function definition(){
        global $CFG, $DB, $USER;
        $mform = $this ->_form;
        $formtable = 'feedback_form';
        //$visibilityArray = array(0 =>'Public',1=>'Private');
        $formid = $_GET['id'];
        $formstuff =$DB->get_record($formtable, array('id'=>$formid)); 

        $mform->addElement('header','addCategoryHeader',get_string('addCategoryHeader', 'local_rubricrepo_kboyle'));
        if($formstuff->visibility == 0){
             $mform->addElement('static','formStatus0',get_string('editFormStatus', 'local_rubricrepo_kboyle'),get_string('isItpub', 'local_rubricrepo_kboyle'));
        }else{
            $mform->addElement('static','formStatus1',get_string('editFormStatus', 'local_rubricrepo_kboyle'),get_string('isItpriv', 'local_rubricrepo_kboyle'));

        }
       
        $mform->addElement('submit', 'addChangePermissionsButton', get_string('changePermission', 'local_rubricrepo_kboyle'));
        //$this->add_action_buttons($cancel=true, $sumitlabel = get_string('saveIt', 'local_rubricrepo_kboyle'));
    }

};


?>