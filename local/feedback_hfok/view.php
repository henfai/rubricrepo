<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the feedback form
 *
 * @package     local
 * @subpackage  feedback_hfok
 * @copyright   hfok Pokharel hfok@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
require_once('../../config.php');
global $CFG, $PAGE;

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title('Form name');
$PAGE->set_heading('Form name');
$PAGE->set_url($CFG->wwwroot.'/local/feedback_hfok/view.php');
echo $OUTPUT->header();
**/

global $CFG, $PAGE;
require_once('../../config.php');
//require_once('lib.php');
//require_once($CFG->libdir.'/completionlib.php');

require_login();
require_capability('local/feedback_hfok:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_hfok/course_selection_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_hfok'));
//$PAGE->set_title($USER->name);
$PAGE->set_heading(get_string('pluginname', 'local_feedback_hfok'));
//$PAGE->set_heading($USER->name);
$PAGE->set_url($CFG->wwwroot.'/local/feedback_hfok/view.php');
$courselist_form = new create_course_list();

if ($courselist_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/feedback_hfok/view.php');
} elseif ($data = $courselist_form->get_data()) {
	//$check = $data->test1;
	redirect($CFG->wwwroot.'/local/feedback_hfok/feedback.php');
} else {
	//$PAGE->set_title($forum->name);
    //$PAGE->add_body_class('forumtype-'.$forum->type);
    //$PAGE->set_heading($course->fullname);

	echo $OUTPUT->header();
	$courselist_form->display();
	echo $OUTPUT->footer();
}

?>
