<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_hfok' to pick a course
 *
 * @package     local
 * @subpackage  feedback_hfok
 * @copyright   hfok Pokharel hfok@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once $CFG->dirroot.'/lib/formslib.php';

class create_course_list extends moodleform {
    
    function definition() {
        global $CFG, $COURSE, $DB, $USER;
        $mform =& $this->_form;
        $attributes_heading = array('size' => '30');
        $attributes =array('size'=>'18', 'maxlength' => '100');
        $mform->addElement('header', 'heading1', get_string('courseheading', 'local_feedback_hfok'), $attributes_heading);
        
        $user_firstname = $USER->firstname;
        $user_lastname = $USER->lastname;
        $user_fullname = $user_firstname.' '.$user_lastname;
        //echo $user_fullname;
        $user_email = $USER->email;
        
        
        //------------------FOR NEW FUNCTION---------------------------------------------
        //print_object($COURSE);
        //SELECT 'fullname' FROM COURSES WHERE 'id' = CURRENT COURSE ID ===> this is what the next line does
        //$courses = $DB->get_record('course', array('id'=>$COURSE->id), 'fullname', MUST_EXIST);
        //print_r($courses);
        
        /*Get enrolled courses of user by user id
            Source: http://codeboxr.com/blogs/moodle-development-tips-tricks-part1
            Author: Sabju Kundu
        */
        $user_courses = enrol_get_users_courses($USER->id, true, '*', 'visible DESC,sortorder ASC');
        $select_course_array=array();
        foreach ($user_courses as $uc){
            $select_course_array[$uc->id] = $uc->fullname;
        }
        $selectgroup = array();
        $selectgroup[] = $mform->createElement('select', 'course', get_string('choosecourse', 'local_feedback_hfok'), $select_course_array);
        $mform->addElement('group', 'courseselect', get_string('choosecourse', 'local_feedback_hfok'), $selectgroup, array(' '), false);

        //$allcourses = get_courses(); gets all the courses in the database and stores in an araray
        //print_r($allcourses[1]); prints info about the course with id 1
        //$course_name = $allcourses[3]->fullname; prints the fullname of the course with id 3
        /** Option 2
        $name = $_GET['name'];
        echo $name ;*/
        
        $mform->addElement('header', 'heading2', get_string('forumheading', 'local_feedback_hfok'), $attributes_heading);
        $table1= 'forum';
        $discussion = $DB->get_records($table1, array('course'=>'2'));
        $arrgroup = array();
        foreach($discussion as $d) {
            $arrgroup[$d->id] = $d->name;
        }

        $selectgroup = array();
        $selectgroup[] = $mform->createElement('select', 'forum', get_string('forumheading', 'local_feedback_hfok'), $arrgroup);
        //$selectgroup[] = $mform->createElement('submit', 'forumsearch', get_string('load', 'local_feedback_hfok'));
        $mform->addElement('group', 'selectforum', get_string('forumheading', 'local_feedback_hfok'), $selectgroup, array('  '), false);
        ////////////TESTINGGGGGGGG !!~~#@#@$@$$#%$#$%$#

        /*$table1 = 'forum';
        $mform->addElement('header', 'forumheader', get_string('forumheading', 'local_feedback_hfok'));
        //$discussion = $DB->get_records($table1, array('course'=>'2', 'type'=>'general'));
        $discussion = mod_forum_get_forums_by_courses();
        $arrgroup = array();

        foreach($discussion as $d) {
            $arrgroup[$d->id] = $d->name;
        }
        

        $selectgroup = array();
        $selectgroup[] = $mform->createElement('select', 'forum', get_string('forumheading', 'local_feedback_hfok'), $arrgroup);
        $selectgroup[] = $mform->createElement('submit', 'forumsearch', get_string('load', 'local_feedback_hfok'));
        $mform->addElement('group', 'selectforum', get_string('forumheading', 'local_feedback_hfok'), $selectgroup, array('  '), false);
    */
        //$mform->addElement('header', 'heading3', get_string('postheading', 'local_feedback_hfok'), $attributes_heading);
        $table2 = 'forum_discussions';
        $user_table = 'user';
        $posts = $DB->get_records($table2, array('forum'=>'3'));
        $table = new html_table();
        $table->head = array('   Post Title   ', '   Author   ');
        //TABLE UI STYLE ELEMENTS FOR ALIGNMENT AND COLUMN WIDTH
        $table->tablealign = 'center';
        $table->width = '75%';

        //print_r($posts);
        //echo '<br><br>';
        foreach($posts as $p) {
            $post_id = $p->id;
            $post_title = $p->name;
            $author_id = $p->userid;
            $select = "id ='".$author_id."'";
            $author_info = $DB->get_records_select($user_table, $select);
            $author_firstname = $author_info[$author_id]->firstname;
            $author_lastname = $author_info[$author_id]->lastname;
            $author_fullname = $author_firstname.' '.$author_lastname;
            $table->data[] = array('<a href=view.php?id='.$post_id.'>'.$post_title.'</a>', $author_fullname);
        }

        echo html_writer::table($table);
        

       

        $this->add_action_buttons();
    }

}

?>

