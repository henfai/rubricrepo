<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the feedback form
 *
 * @package     local
 * @subpackage  feedback_hfok
 * @copyright   Eric Cheng ec10@ualberta.ca && hfok Pokharel hfok@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 * Example:
 * require_once('../../config.php');
 * global $CFG, $PAGE;
 *
 * $PAGE->set_context(context_system::instance());
 * $PAGE->set_pagelayout('standard');
 * $PAGE->set_title('Form name');
 * $PAGE->set_heading('Form name');
 * $PAGE->set_url($CFG->wwwroot.'/local/feedback_hfok/view.php');
 * echo $OUTPUT->header();
**/

global $CFG, $PAGE, $DB;
require_once('../../config.php');

require_login();
require_capability('local/feedback_hfok:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_hfok/feedback_form.php');
require_once($CFG->dirroot.'/local/feedback_hfok/lib.php');

// This block handles the page generation and set up.
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_hfok'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_hfok'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_hfok/feedback.php');
$feedback_search = new feedback_form_search();

//$feedback_form = new create_feedback_instance($CFG->wwwroot.'/local/feedback_hfok/feedback.php?id='.$_GET['id']);

/* This block handles the logic for how to handle the form.
 * 1. If the form is cancelled, re-direct back to this page.
 * 2. If the form is submitted and the information is valid, then process the form.
 * 2a. If the submitted form has no feedback selected, the user is re-directed back to the page and an error is displayed.
 * 3. If there is no form information, then display the newly created form.
**/

//$table1 = 'comments';
/*
if ($feedback_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/feedback_hfok/feedback.php?id='.$_GET['id']);
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST' && $feedback_form->get_data()) { //Double guard to ensure that the requesting method is POST and that form data is valid.
	$valid_form = $feedback_form->checkbox_validation(); // Call to custom validation function.
	if (!$valid_form) {
		redirect($CFG->wwwroot.'/local/feedback_hfok/feedback.php?id='.$_GET['id'].'&invalid=1');
	}
	save_comment_serials();
	/*
	$grp_count = 0;
	$chkboxgrps = 'grp'.$grp_count;
	while (!empty($_POST[$chkboxgrps])) {
		//echo 'Checkboxes selected<br>';
		$checkbox = $_POST[$chkboxgrps];
		//echo $selected.'<br>';
		foreach($checkbox as $c) {
			//echo $c;
			if ($c != 0) {
				$result = $DB->get_record($table1, array('id'=>$c));
				echo $result->comment_text.'<br>';
			}
		}
		$grp_count++;
		$chkboxgrps = 'grp'.$grp_count;
	}
} else {
*/
	//echo 'formid is'.$_GET['formid'].'<br>';
	//echo 'id is'.$_GET['id'].'<br>';
	//echo 'id as POST is'.$_POST['id'].'<br>';
	//echo 'courseid is'.$_GET['courseid'].'<br>';
	echo $OUTPUT->header();
	$feedback_search->display();
	echo $OUTPUT->footer();
//}

?>
