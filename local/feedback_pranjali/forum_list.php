<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_pranjali'
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca && Eric Cheng ec10@ualberta.ca 
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';


/**
 * The search engine that allows the user to select the form to use for marking.
 */
class create_discussion_list extends moodleform {
	function definition() {
		global $CFG, $DB;
		$mform = $this->_form;
		$table1 = 'forum';
		//$id = 0;
		$id = $_GET['cid'];
		//$id = 2;
		//echo $id.'is of type '.gettype($id).'<br>';
		$arrgroup = array();
		//$arrgroup[0] = '';
		
		$discussion = $DB->get_records($table1, array('course'=>$id, 'type'=>'general'));

		foreach($discussion as $d) {
			$arrgroup[$d->id] = $d->name;
		}
		
		$selectgroup = array();
		$selectgroup[] = $mform->createElement('header', 'forumheader', get_string('useforum', 'local_feedback_pranjali'));
		$selectgroup[] = $mform->createElement('select', 'forum', get_string('forumlist', 'local_feedback_pranjali'), $arrgroup);
		$selectgroup[] = $mform->createElement('submit', 'search', get_string('forumsearch', 'local_feedback_pranjali'));
		$mform->addGroup($selectgroup, 'selectforum', get_string('selectforum', 'local_feedback_pranjali'), '  ', false);

	}
}

?>