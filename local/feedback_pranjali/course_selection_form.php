<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_pranjali' to pick a course
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca 
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';

/*
 * This page allows the instructor to select the course and the discussion thread to mark
 * The discussion thread posts are displayed in a table from which the instructor can pick one
 */

class create_course_list extends moodleform {
    function definition() {
        global $CFG, $COURSE, $DB, $USER;
        $mform =& $this->_form;
        $attributes_heading = array('size' => '30');
        $mform->addElement('header', 'courseheader', get_string('useforms', 'local_feedback_pranjali'), $attributes_heading);
        
        /*Get enrolled courses of user by user id
            Source: http://codeboxr.com/blogs/moodle-development-tips-tricks-part1
            Author: Sabju Kundu
        */
        $user_courses = enrol_get_users_courses($USER->id, true, '*', 'visible DESC,sortorder ASC');
        $select_course_array=array();
        $select_course_array[0] = ''; //Blank space is added to the select box
        foreach ($user_courses as $uc){
            $select_course_array[$uc->id] = $uc->fullname;
        }
        $selectgroup = array();
        $selectgroup[] = $mform->createElement('select', 'course', get_string('selectcourse', 'local_feedback_pranjali'), $select_course_array);
        $selectgroup[] = $mform->createElement('submit', 'search', get_string('coursesearch', 'local_feedback_pranjali'));
        $mform->addElement('group', 'courseselect', get_string('selectcourse', 'local_feedback_pranjali'), $selectgroup, array('style'=>'width:75%;'), false);

    }
}

/**
 * Select the discussion forum that a certain post being marked is contained in 
 */
class thread_search_engine extends moodleform {
	function definition() {
		global $DB, $CFG;
		$mform = $this->_form;
		$attributes_heading = array('size' => '30');
		$fid = $_GET['fid'];
		$mform->addElement('header', 'heading3', get_string('postheading', 'local_feedback_pranjali'), $attributes_heading);
        $table2 = 'forum_discussions';
        $user_table = 'user';
        $posts = $DB->get_records($table2, array('forum'=>$fid));
        $table = new html_table();
        $table->head = array('   Post Title   ', '   Author   ');
        $table->align = 'center';
        $table->width = '100%';

        foreach($posts as $p) {
            $post_id = $p->id;
            $post_title = $p->name;
            $author_id = $p->userid;
            $select = "id ='".$author_id."'";
            $author_info = $DB->get_records_select($user_table, $select);
            $author_firstname = $author_info[$author_id]->firstname;
            $author_lastname = $author_info[$author_id]->lastname;
            $author_fullname = $author_firstname.' '.$author_lastname;
            $table->data[] = array('<a href=view.php?id='.$post_id.'>'.$post_title.'</a>', $author_fullname);
        }

        $mform->addElement('html', $table);
	}
}

/*
 * Function to display the thread as a table with post title and author name as columns
 * This shows the table in the page below the discussion selection rather than at the top with the header
 * if mform is added then echo-ing the table outputs the table at the top rather than in the page
 */
function display_thread_search() {
	global $DB, $CFG;
	$attributes_heading = array('size' => '30');
	$fid = $_GET['fid'];
    $table2 = 'forum_discussions';
    $user_table = 'user';
    $posts = $DB->get_records($table2, array('forum'=>$fid));
    $table = new html_table();
    $table->head = array('   Post Title   ', '   Author   ');
    $table->align = 'center';
    $table->width = '100%';

    foreach($posts as $p) {
        $post_id = $p->firstpost;
        $post_title = $p->name;
        $author_id = $p->userid;
        $select = "id ='".$author_id."'";
        $author_info = $DB->get_records_select($user_table, $select);
        $author_firstname = $author_info[$author_id]->firstname;
        $author_lastname = $author_info[$author_id]->lastname;
        $author_fullname = $author_firstname.' '.$author_lastname;
        $table->data[] = array('<a href=feedback.php?id='.$post_id.'>'.$post_title.'</a>', $author_fullname);
    }

    echo html_writer::table($table);
}

?>

