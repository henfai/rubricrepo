<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic and displays the initial page
 * From  this page the instructor can select course, discussion, and thread post to mark
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca && Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
**/

global $CFG, $PAGE, $DB;
require_once('../../config.php');

require_login();
require_capability('local/feedback_pranjali:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_pranjali/course_selection_form.php');
require_once($CFG->dirroot.'/local/feedback_pranjali/thread_search_engine.php');
require_once($CFG->dirroot.'/local/feedback_pranjali/rubric_manager_form.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/view.php');

$table1 = 'forum';
$table2 = 'forum_discussions';
$course_search = new create_course_list();
$rubric_manager = new rubric_manager();

/**
 * This block handles whether or not the discussion search engine should be displayed based on the header POST information AND if there are any discussions
 * that corresponds to the course selected.
 */
if (!$_GET['id']) {
	$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/view.php');
} else {
		$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/view.php?id='.$_GET['id']);	
		if ($DB->get_records($table1, array('course'=>$_GET['id'], 'type'=>'general'))) {
			$discussion_search = new discussion_forum_search_engine($CFG->wwwroot.'/local/feedback_pranjali/view.php?id='.$_GET['id']);
	}
}

/**
 * This block handles the logic for how to handle user submissions when selecting the course, discussion, and form.
 * 1. If all the search form is valid then POST all the information and re-direct to the corresponding form page.
 * 2. There is no form information, display the default screen.
 */


if ($_POST['search']) {
	$data = $course_search->get_data();
	redirect($CFG->wwwroot.'/local/feedback_pranjali/view.php?id='.$data->course);
} else if ($_POST['createNR']) {
	redirect($CFG->wwwroot.'/local/feedback_pranjali/creation.php');
} else if ($_POST['editER']) {
	redirect($CFG->wwwroot.'/local/feedback_pranjali/edit.php');
} else if ($_POST['deleteR']) {
	redirect($CFG->wwwroot.'/local/feedback_pranjali/edit.php');
} else if ($_POST['submitbuttonforum']) {
	$data = $discussion_search->get_data();
	redirect($CFG->wwwroot.'/local/feedback_pranjali/view.php?id='.$_GET['id'].'&fid='.$data->forum_search_engine);
} else {
	echo $OUTPUT->header();
	$rubric_manager->display();
	$course_search->display();
	if($_GET['id']) {
		$discussion_search->display();
	}
	if ($_GET['fid'] && $DB->get_records($table2, array('forum'=>$_GET['fid']))) {
		display_thread_search();
	}
	echo $OUTPUT->footer();
}

?>
