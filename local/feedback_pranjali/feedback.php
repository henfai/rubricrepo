<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the feedback form
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Eric Cheng ec10@ualberta.ca && Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 * Example:
 * require_once('../../config.php');
 * global $CFG, $PAGE;
 *
 * $PAGE->set_context(context_system::instance());
 * $PAGE->set_pagelayout('standard');
 * $PAGE->set_title('Form name');
 * $PAGE->set_heading('Form name');
 * $PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/view.php');
 * echo $OUTPUT->header();
**/

global $CFG, $PAGE, $DB;
require_once('../../config.php');

require_login();
require_capability('local/feedback_pranjali:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_pranjali/feedback_form.php');
require_once($CFG->dirroot.'/local/feedback_ec10/lib.php');

// This block handles the page generation and set up.
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_pranjali'));

if (!$_GET['rid']) {
	$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id']);
	$feedback_search = new feedback_form_search($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id']);
} else {
	$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$_GET['rid']);
	$feedback_search = new feedback_form_search($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$_GET['rid']);
}

if ($_GET['rid'] && !$_GET['invalid']) {
	$feedback_form = new create_feedback_instance($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$_GET['rid']);
} else if ($_GET['rid'] && $_GET['invalid']) {
	$feedback_form = new create_feedback_instance($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$_GET['rid'].'&invalid=1');
}


/**
 * This block handles the logic for how to handle the search engine and what gets displayed.
 * 1. If a form is selected, re-load the page with the form data displayed.
 * 2. If the form is submitted then:
 * 2a. Two conditions to check for the value of 'invalid' in the POST header.  This is because the $CFG information that gets passed into the the validation check, must
 *     correspond with the page that is currently displayed.
 * 2b. If the form is invalid, then re-direct back to this page.
 * 2c. If the form is valid, then save the form and re-direct to the e-mail preview page.
 * 3. Display the page based on the header information.
 */

if ($_POST['selectfeedbackrubric']) {
	$data = $feedback_search->get_data();
	redirect($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$data->form);
} else if ($_POST['submitformbutton']) {
	if(!$_GET['invalid']) {
		$valid_form = $feedback_form->checkbox_validation($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$_GET['rid']);
	} else {
		$valid_form = $feedback_form->checkbox_validation($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$_GET['rid'].'&invalid=1');
	}
	if (!$valid_form) {
		redirect($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id'].'&rid='.$_GET['rid'].'&invalid=1');
	} else {
		//echo 'Form is valid.<br>';
		//var_dump($_POST);
		$saved_form_id = save_completed_form();
		//echo $saved_form_id;
		redirect($CFG->wwwroot.'/local/feedback_pranjali/email_preview.php?id='.$saved_form_id);
	}
} 
else {
	echo $OUTPUT->header();
	$feedback_search->display();
	display_thread($CFG->wwwroot.'/local/feedback_pranjali/feedback.php?id='.$_GET['id']);
	if ($_GET['rid']) {
		$feedback_form->display();
	}
	echo $OUTPUT->footer();
}

?>
