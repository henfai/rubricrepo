<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_pranjali'
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca && Eric Cheng ec10@ualberta.ca 
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';

class discussion_forum_search_engine extends moodleform {

	function definition() {
		global $CFG, $DB;
		$mform = $this->_form;
		$table1 = 'forum';
		$id = $_GET['id'];
		$results = $DB->get_records($table1, array('course'=>$id, 'type'=>'general'));
		$select_array = array();

		foreach($results as $r) {
			$select_array[$r->id] = $r->name;
		} 

		$element_group = array();
		$mform->addElement('header', 'header1', get_string('useforum', 'local_feedback_pranjali'));
		$element_group[] = $mform->createElement('select', 'forum_search_engine', get_string('forumlist', 'local_feedback_pranjali'), $select_array);
		$element_group[] = $mform->createElement('submit', 'submitbuttonforum', get_string('forumsearch', 'local_feedback_pranjali'));
		$mform->addElement('group', 'forumsearchengine', get_string('selectforum', 'local_feedback_pranjali'), $element_group, array('   '), false);

	}
}
?>