<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'feedback_pranjali', language 'en'
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['sendfeedback'] = 'Send Feedback';
$string['menuoption'] = 'pranjali Feedback Plugin (DEV build)';
$string['pluginname'] = 'pranjali Feedback Plugin (DEV)';
$string['feedback_pranjali:add'] = 'You do not have permission to use the feedback form';
$string['student'] = 'Student Name';
$string['email'] = 'Student E-mail';
$string['subject'] = 'Discussion Subject';
$string['freetext'] = 'Comments';
$string['field_required'] = 'This is a required field';
$string['submitbuttonforum'] = 'Search';
$string['selectform'] = 'Select a feedback rubric';
$string['selectcourse'] = 'Select a course';
$string['useforms'] = 'Provide Feedback To Discussions';
$string['load'] = 'Load';
$string['coursesearch'] = 'Search for forums';
$string['forumsearch'] = 'Search for threads';
$string['feedbacksearch'] = 'Load rubric';
$string['selectforum'] = 'Select a forum';
$string['forumlist'] = 'Forum list';
$string['useforum'] = 'Select A Forum To Provide Feedback To';
$string['postheading'] = 'Select A Thread To Give Feedback To';
$string['feedbacksearchengine'] = 'Selected feedback rubric';
$string['submitform'] = 'Submit';
$string['positive_category'] = 'Positive Feedback';
$string['negative_category'] = 'Negative Feedback';
$string['public'] = 'Public';
$string['private'] = 'Private';
$string['access'] = 'Accessibility';
$string['name_field'] = 'Name';
$string['addCategoryDesc'] = 'Add the categories in which you wish to submit feedback for';
$string['classification'] = 'Category Classification';
$string['continue'] = 'Continue';
$string['formInfo'] = 'General feedback form information';
$string['add_comment'] = 'Add comment';
$string['new_comment'] = 'New comment';
$string['association'] = 'Select associated category';
$string['add_categories'] = 'Add a category';
$string['createformheader'] = 'Cannot find the feedback rubric that you are looking for?';
$string['createbutton'] = 'Create new Rubric';
$string['editbutton'] = 'Edit existing Rubric';
$string['deletebutton'] = 'Delete Rubric';
$string['manage'] = 'Manage My Feedback Rubrics';

/**
 * The following strings are used to define errors to the user.
 */
$string['error'] = 'An error occurred';
$string['noformtext'] = 'The form you have selected does not exist';
$string['noboxeschcked'] = 'Invalid form error: Cannot submit an empty feedback form.';

/**
 * Kieran's strings for the e-mail page.
 */
$string['savechanges'] = 'submit';
$string['emailpreview'] = ' Email Preview';
$string['graderName'] = 'Professor/TA';
$string['defaultGrader'] = "Dr. Branch-Mueller";
$string['emailSubjectHeader'] = 'Subject';
$string['empty'] = '';
$string['studentInfo'] = 'Student Information';
$string['pros'] = 'Pros';
$string['cons'] = 'Cons';
//$strings['info'] = 'Post and Marker Information';
$string['contextInformation'] = 'Post and Marker Information';
$string['greeting'] = 'Dear ';
$string['comma'] = ',';
$string['thanks'] = 'Thanks for your scholarly contribution to: '; 
$string['thisWeek'] = 'this week.  I really liked: '; 
$string['goodStuff'] = 'You have met the excellent performance level in the following areas: ';
$string['badStuff'] = 'For next time try to: ';
$string['finalWords'] =  'Have a great day, and please get in touch if you have any questions or concerns.';
//$string['error'] = 'An error occurred';
$string['noformtext'] = 'The form you have selected does not exist';

//ERIC's STRINGS FOR RUBRIC MANAGEMENT

$string['manageHeader'] = 'Manage';
$string['useHeader'] = 'Use';
$string['createRubric'] = 'Create';
$string['deleteRubric'] = 'Delete';
$string['formName'] = ' Form Name';
$string['sendEmail'] = 'Send';
$string['numCategories'] = 'Enter the number of categories you wish to have';
$string['numPostives'] = 'Enter the number of positive comments you have about this category';
$string['numNegatives'] = 'Enter the number of negative comments you have about this category';
$string['removeCategory'] = 'Select a category which you wish to remove';
$string['addCategory'] = 'Add category';
$string['nextPage'] = 'Next';


?>