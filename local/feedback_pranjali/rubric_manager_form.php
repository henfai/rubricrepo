<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The form used by 'feedback_pranjali'
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';

/**
 * The search engine that allows the user to select the form to use for marking.
 */
class rubric_manager extends moodleform {
	function definition() {
		$mform = $this->_form;



		$mform->addElement('header', 'management_header', get_string('manage', 'local_feedback_pranjali'));
                $mform->setExpanded('management_header', false);
		$mform->addElement('html', '<br><a href ="http://199.116.235.182/dev/pranjali/rubricrepo/local/feedback_pranjali/creation.php" target="_parent"> Create new Rubric &emsp;&emsp;&emsp;</a>');
		$mform->addElement('html', '<a href ="http://199.116.235.182/dev/pranjali/rubricrepo/local/feedback_pranjali/edit.php" target="_parent"> Edit existing Rubric &emsp;&emsp;&emsp;</a>');
		$mform->addElement('html', '<a href ="http://199.116.235.182/dev/pranjali/rubricrepo/local/feedback_pranjali/edit.php" target="_parent"> Delete Rubric </a></br>');

	}
}

