<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca && Eric Cheng ec10@ualberta.ca && Sabrina Gannon sgannon@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */


global $CFG, $PAGE;
require_once('../../config.php');

require_login();
require_capability('local/feedback_pranjali:add', context_system::instance());
require_once($CFG->dirroot.'/local/feedback_pranjali/email_preview_form.php');
require_once($CFG->dirroot.'/lib/moodlelib.php');

$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_heading(get_string('pluginname', 'local_feedback_pranjali'));
$PAGE->set_url($CFG->wwwroot.'/local/feedback_pranjali/email_preview.php?id='.$_GET['id']);
$email_form = new create_email_instance($CFG->wwwroot.'/local/feedback_pranjali/email_preview.php?id='.$_GET['id']);

$thread = $DB->get_record('saved_form',array('id'=>$_GET['id']));
$forumpost = $DB->get_record('forum_posts',array('id'=>$thread->postid)); 
$topic = $forumpost->subject;
//$data = $email_form->get_data();
/**
 * These are the MINIMUM parameters needed for sending off an e-mail.
 * You can easily just use $USER for the $sentBy parameter and get the student based off the student ID in the system.
 * The student ID can be extracted from the saved_form table, which references the ID of the student that made the initial msg of the thread that was selected.
 */
$emailUser = new stdClass();
$emailUser->email = $USER->email;
$emailUser->firstname = $USER->firstname;
$emailUser->lastname = $USER->lastname;
$emailUser->maildisplay = true;
$emailUser->mailformat = 0;
$emailUser->firstnamephonetic = '';
$emailUser->lastnamephonetic = '';
$emailUser->middlename = '';
$emailUser->alternatename = '';
$emailUser->id = $USER->id;

/*
* This code is for loading the email page and displaying the contents
* and is responsible for  for redirecting and displaying the header and
* the footer
*  
*/
if ($email_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/feedback_pranjali/view.php');
} else if ($data = $email_form->get_data()) {
	/**
	 * This does not actually send an e-mail because of SMTP issues.
	 * The method email_to_user() is part of the moodlelib.php and requires at least 
	 */
	$data = $email_form->get_data();
	$msg = $data->content;
	var_dump($emailUser);
	//echo $data->content.'<br>';
	//echo $topic;
	$sent = email_to_user($emailUser, $emailUser, $topic, $msg);
	var_dump($sent);
} else {
	echo $OUTPUT->header();
	display_email();
	$email_form->display();
	echo $OUTPUT->footer();

}

?>